---
# CoSim20 Framework: Python Version

## Installation
### Requirements
- Python
- `pip`

It is possible to use `virtualenv` to create a virtual environment. On Ubuntu:
```
# Install pip 
> sudo apt-get install python3-pip
# Then install virtualenv using pip3
> sudo pip3 install virtualenv
# Create a new virtual environment 
> virtualenv venv
# Activate the virtual environment
> source venv/bin/activate
# Install the requirements
> pip install -r requirements.txt 
```
# Getting started

## Example 1: Fault Injection

Simple example to demonstrate the modular capabilities of the cosim20 framework. The first example can be found in `examples/fault-injection`
The file `actions.scenario` is a list of faults to introduce at specific points in time. For instance, 
```
@7000.0 set fanIsBroken 1
```
the instance injects (`set`) at instant `7000` the value `1` on the model port `fanIsBroken` on the target simulation unit. 

Setup the env variables to match the exposed ports from the target model:
- `SYNC_PORT` : ZeroMQ Synchronization port 
- `PORT` : ZeroMQ Exchange port

On Ubuntu:
```
> export SYNC_PORT=<TCP/UDP-port-number>
> export PORT=<TCP/UDP-port-number>
```
Launch the program:
```
> python3 cosim-example.py
```

# Project structure

```
|-- examples                # Directory containing examples
    |-- fault-injection     # Example emulating a fault injection executable model
|-- predicates              # Sketch for predicates in Python
|-- commands                # Sketch for commands in Python
```