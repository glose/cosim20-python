from abc import ABC


class CoordinationPredicate(ABC):

    def __init__(self):
        self.isVerified = False

    def getTemporalPredicate(self):
        pass

    def getLogicalStepPredicate(self):
        pass