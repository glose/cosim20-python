from predicates.CoordinationPredicate import CoordinationPredicate


class ReadyToReadPredicate(CoordinationPredicate):
    def __init__(self):
        super().__init__()
