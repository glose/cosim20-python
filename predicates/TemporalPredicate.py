from predicates.Predicate import Predicate


class TemporalPredicate(Predicate):
    def __init__(self, deltaT):
        super().__init__()
        self.deltaT = deltaT

    def getTemporalPredicate(self):
        return self