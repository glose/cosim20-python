from abc import ABC


class Predicate(ABC):

    def __init__(self):
        self.isVerified = False

    def get_temporal_predicate(self):
        pass

    def get_logical_step_predicate(self):
        pass