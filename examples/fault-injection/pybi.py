import csv
import json
import time

from zmq.utils.strtypes import b

import PyModel


class PythonBI:
    def __init__(self, socket):
        self.socket = socket
        with open('actions.scenario', mode='r') as actionsfile:
            self.csv_reader = csv.DictReader(actionsfile)

    def doStep(self):
        line_count = 0
        for row in self.csv_reader:
            if line_count == 0:
                pass
            else:
                topic = "inputs"
                messagedatarow = {
                    "id": row['variable'],
                    "temporalHorizon": row['time'],
                    "bvalue": row['value']
                }
                messaged = json.dumps(messagedatarow)
                self.socket.send_multipart([b(topic), b(messaged)])
                print("Sent %s %s", topic, messaged)
                time.sleep(1)
                # Send timestamp, variable and value on the queue
            line_count += 1


    def run(self):
        model = PyModel()
        while model.isTerminated():
            # Build Predicate

            # Run the Predicate

            # Send


